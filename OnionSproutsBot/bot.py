#!/usr/bin/env python3
#
# This file is part of OnionSpoutsBot, a Tor Browser distribution Telegram bot.
#
# :authors: Panagiotis "Ivory" Vasilopoulos <git@n0toose.net>
#           Evangelos "GeopJr" Paterakis <evan@geopjr.dev>
#           Justin "Justasic" Crawford <OpenSource@stacksmash.net>
#
# :copyright:   (c) 2020-2022, Panagiotis "Ivory" Vasilopoulos
#
# :license: This is Free Software. See LICENSE for license information.
#

from distutils.util import get_platform
from . import database, files, titles, ui, helpers, i18n

from aiologger import Logger
from aiologger.formatters.base import Formatter
from aiologger.handlers.files import AsyncFileHandler
from aiologger.handlers.streams import AsyncStreamHandler
from time import time
from datetime import datetime as dt
from pathlib import Path
from requests import HTTPError
from pyrogram import idle, Client, filters
from pyrogram.errors.exceptions.bad_request_400 import QueryIdInvalid
from pyrogram.types import (InlineQueryResultArticle, InputTextMessageContent,
                            InlineKeyboardMarkup, InlineKeyboardButton,
                            KeyboardButton, ReplyKeyboardMarkup, CallbackQuery,
                            Message, User)

import aiosqlite
import asyncio
import logging
import os
import requests
import sys
import yaml
import typing

# TODO: Better argument interface
with open(sys.argv[1], "r") as config:
    data = yaml.safe_load(config)

OnionSproutsBot = Client(
    "OnionSproutsBot",
    data['telegram']['api_id'],
    data['telegram']['api_hash'],
    bot_token=data['telegram']['bot_token'],
    in_memory=True
)

i18n.setup_gettext()
logger = Logger(name = "OnionSproutsBot")
bot_profile: typing.Union[User, None] = None

@OnionSproutsBot.on_message(filters.command("start"))
async def start_command(client: Client, message: Message):
    user = message.from_user
    # User is not provided if the message is in a channel.
    if not user:
        return None

    # Check if the language returned by Telegram is available.
    # If it is, send the welcome message, else send the language menu.
    user_lang = user.language_code.lower()
    available_locales = i18n.available_locales.keys()
    if user_lang != 'en' and user_lang in available_locales:
        return await send_welcome_message(client, message, user_lang)

    await send_language_menu(client, message, user_lang)

# Sends the lanuage menu
async def send_language_menu(client: Client, message: typing.Union[Message, CallbackQuery], user_lang: str):
    # Set the translation to user_lang.
    _ = i18n.get_translation(user_lang)

    lang_rows = []
    available_locales = i18n.available_locales.keys()
    # The sort function is being used to reorder the list so user.language_code
    # is the first button, in a way to make it easier for the user to pick
    # their language if Telegram returned the correct one.
    for lang in sorted(available_locales,key=user_lang.__eq__, reverse=True):
        lang_rows.append(InlineKeyboardButton(
            text=f'{i18n.available_locales[lang]["full_name"]} ({lang})',
            callback_data='welcome:' + lang
            )
        )

    button_rows = ui.get_rows(lang_rows, 3)

    lang_markup = InlineKeyboardMarkup(button_rows)
    photo='https://gitlab.torproject.org/tpo/web/community/-/raw/f8ccce8c64961e48a749eff62c38343214014a35/assets/static/images/home/png/localization.png'
    caption=_("<strong>Language selection</strong>\n\n"
            "Please select your language:"
    )

    if isinstance(message, Message):
        await client.send_photo(chat_id=message.chat.id, photo=photo, caption=caption, reply_markup=lang_markup)
    else:
        # if there is no photo on this message, delete it and start over.
        if not message.message.photo:
            await asyncio.gather(
                client.send_photo(chat_id=message.message.chat.id, photo=photo, caption=caption, reply_markup=lang_markup),
                message.message.delete()
            )
        else:
            await message.message.edit(text=caption, reply_markup=lang_markup)


# Example query: change_lang:en
@OnionSproutsBot.on_callback_query(filters.regex("^change_lang:[^:]+$"))
async def change_lang(client: Client, callback: CallbackQuery):
    # All callbacks need to get the language from now on.
    callback_data = callback.data.split(':')
    lang = callback_data[-1]

    await send_language_menu(client, callback, lang)

# Example query: welcome:en
@OnionSproutsBot.on_callback_query(filters.regex("^welcome:[^:]+$"))
async def welcome_command(client: Client, callback: CallbackQuery):
    # All callbacks need to get the language from now on.
    callback_data = callback.data.split(':')
    lang = callback_data[-1]

    await send_welcome_message(client, callback, lang)

# Sends the welcome message
async def send_welcome_message(client: Client, message: typing.Union[Message, CallbackQuery], lang: str):
    global bot_profile
    _ = i18n.get_translation(lang)

    # If lang is English, label = 'Change Language 🌐'
    # else label = "<'Change Language' translated> (Change Language) 🌐"
    if lang.startswith('en_') or lang == 'en':
        change_lang_button_label = 'Change Language 🌐'
    else:
        change_lang_button_label = _("Change Language") + " (Change Language) 🌐"

    text=_("<strong>Welcome!</strong>\n\n"
               "Hi, welcome to {name}! "
               "What do you want me to do?"
    ).format(name = bot_profile.first_name)

    reply_markup=InlineKeyboardMarkup(
    [
            [InlineKeyboardButton(
                _("Send me Tor Browser through Telegram"),
                "request_tor:" + lang
            )],
            [InlineKeyboardButton(
                _("Send me other mirrors for the Tor Browser"),
                "request_tor_mirrors:" + lang
            )],
            [InlineKeyboardButton(
                _("I want Tor bridges"),
                "request_tor_bridges:" + lang
            )],
            [InlineKeyboardButton(
                _("Explain what Tor is"),
                "explain_tor:" + lang
            )],
            [InlineKeyboardButton(
                change_lang_button_label,
                "change_lang:" + lang
            )]
        ]
    )

    if isinstance(message, Message):
        await client.send_message(chat_id=message.chat.id, text=text, reply_markup=reply_markup)
    else:
        await message.message.edit(text=text, reply_markup=reply_markup)

# Example query: request_tor_mirrors:en
@OnionSproutsBot.on_callback_query(filters.regex("^request_tor_mirrors:[^:]+$"))
async def send_mirrors(client: Client, callback: CallbackQuery):
    callback_data = callback.data.split(':')
    lang = callback_data[-1]
    _ = i18n.get_translation(lang)

    await callback.message.edit(
        text="<strong>Tor Browser Mirrors</strong>\n\n"
        "- EFF: https://tor.eff.org\n"
        "- Calyx Institute: https://tor.calyxinstitute.org\n"
        "- GitHub: https://github.com/torproject/torbrowser-releases/releases/\n"
        "- Email: gettor@torproject.org " + _("(You should also include your operating system: Windows, macOS, or Linux)") + "\n\n" +
        _("(Make sure that your email provider is safe!)") + "\n",
        reply_markup=InlineKeyboardMarkup(
        [
            [InlineKeyboardButton(
                _("Main Menu"),
                "welcome:" + lang
            )]
        ])
    )

# Example query: request_tor_bridges:en
@OnionSproutsBot.on_callback_query(filters.regex("^request_tor_bridges:[^:]+$"))
async def send_mirrors(client: Client, callback: CallbackQuery):
    callback_data = callback.data.split(':')
    lang = callback_data[-1]
    _ = i18n.get_translation(lang)

    await callback.message.edit(
        text=_("<strong>Tor Bridges</strong>\n\n"
               "Tor bridges are computers that can be very helpful when "
               "your internet provider has blocked you from using Tor, "
               "or if you just want to hide the fact that you are using Tor "
               "from anyone that could be monitoring your internet connection. "
               "That could be your internet provider, government, household "
               "members, school, workplace, among others.\n\n"
               "You can request a bridge from @getbridgesbot, but you should "
               "also configure the Tor Browser to use it."
        ),
        reply_markup=InlineKeyboardMarkup(
        [
            [InlineKeyboardButton(
                _("Main Menu"),
                "welcome:" + lang
            )]
        ])
    )


# Example query: explain_tor:en
@OnionSproutsBot.on_callback_query(filters.regex("^explain_tor:[^:]+$"))
async def explain_tor(client: Client, callback: CallbackQuery):
    callback_data = callback.data.split(':')
    lang = callback_data[-1]
    _ = i18n.get_translation(lang)

    await callback.message.edit(
        text=_("<strong>What is Tor?</strong>\n\n"
        "Tor is a network that lets you anonymously connect to the Internet, "
        "while evading censorship, people monitoring of your Internet connection "
        "and ad tracking.\n\n"
        "The network is peer-to-peer and largely supported by volunteers, as "
        "well as The Tor Project Inc., a non-profit organization based "
        "in the United States."),
        reply_markup=InlineKeyboardMarkup(
        [
            [InlineKeyboardButton(
                _("Main Menu"),
                "welcome:" + lang
            )]
        ])
    )

# Example query: request_tor:en
@OnionSproutsBot.on_callback_query(filters.regex("^request_tor:[^:]+$"))
async def request_tor(client: Client, callback: CallbackQuery):
    callback_data = callback.data.split(':')
    lang = callback_data[-1]
    _ = i18n.get_translation(lang)

    platform_keyboard = []
    # Update check on new flow.
    response = await helpers.get_response(data['tor']['endpoint'])

    # Generate list of available platforms, then put
    # each of them in separate buttons.
    for platform in ui.get_platform_list(response):
        platform_keyboard.append(InlineKeyboardButton(
            text=titles.platforms.get(platform.lower(), platform),
            # Here, "select_locale" may be a bit confusing,
            # but here, the buttons tell the bot to proceed
            # with select_locale, while telling it the platform that
            # was chosen.
            callback_data='select_locale:' + platform + ":" + lang
            )
        )

    # After we collect all of the buttons that we should we have,
    # we create our keyboard, and then send it to the user.
    button_rows = ui.get_rows(platform_keyboard, 2)
    platform_markup = InlineKeyboardMarkup(button_rows)

    await callback.message.edit(
        _("<strong>Download Tor from Telegram</strong>\n\n"
        "Okay! I'll ask you a couple of questions first.\n"
        "<b>Which operating system are you using?</b>"),
        reply_markup=platform_markup
    )


# Example query: select_locale:linux64:en
@OnionSproutsBot.on_callback_query(filters.regex("^select_locale:[^:]+:[^:]+$"))
async def select_locale(client: Client, callback: CallbackQuery):
    callback_data = callback.data.split(':')
    lang = callback_data[-1]
    requested_platform = callback_data[1]
    _ = i18n.get_translation(lang)

    locale_keyboard = []

    # Make sure that the platform that was previously selected actually exists.
    platforms = ui.get_platform_list(response)
    if requested_platform.lower() not in platforms:
        return await callback.answer()

    for locale in ui.get_locale_list(response, requested_platform):
        locale_keyboard.append(InlineKeyboardButton(
            text=titles.locales.get(locale.lower(), locale),
            callback_data='download_tor:' + requested_platform + ':' + locale + ":" + lang
            )
        )

    # Separates locales in rows of 4.
    button_rows = ui.get_rows(locale_keyboard, 4)

    locale_markup = InlineKeyboardMarkup(button_rows)
    await callback.message.edit(
        text=_("<strong>Download Tor from Telegram</strong>\n\n"
        "Okay! I'll ask you a couple of questions first.\n"
        "<b>What is your preferred language?</b>"),
        reply_markup=locale_markup
    )

    await callback.answer()

# Example query: download_tor:linux64:it:en
@OnionSproutsBot.on_callback_query(filters.regex("^download_tor:[^:]+:[^:]+:[^:]+$"))
async def download_tor(client: Client, callback: CallbackQuery):
    callback_data = callback.data.split(':')
    lang = callback_data[-1]
    platform = callback_data[1]
    locale = callback_data[2]
    _ = i18n.get_translation(lang)

    await logger.info(f"Binary ({platform}, {locale}) requested.")

    # Checks if the requested platform and locale exist. If not, the
    # whole process is aborted for security reasons.
    if (platform not in ui.get_platform_list(response)) or \
            (locale not in ui.get_locale_list(response, platform)):
        await logger.critical(f"Binary ({platform}, {locale}) NOT in list. Aborting...")
        return await callback.answer()

    # Fetch the download link for the requested files with the response.
    # We do this regardless of whether we already have cached the files
    # or not, as we cannot easily predict the file name and submit a
    # request anyways.
    tor_sig_url = response['downloads'][platform][locale]['sig']
    tor_bin_url = response['downloads'][platform][locale]['binary']

    tor_sig_original_name = tor_sig_url.rsplit('/')[-1]
    tor_bin_original_name = tor_bin_url.rsplit('/', 1)[-1]

    # We check if the file has already been uploaded to Telegram. If so,
    # there should be an entry in our database for it.
    results = await database.search_file_in_db(curs, tor_bin_original_name)

    if results != None:
        await logger.info(f"Binary ({platform}, {locale}) found!")

        await client.send_cached_media(
            callback.from_user.id,
            file_id=results[1]
        )

        # Sends the cached version of the selected signature
        await client.send_cached_media(
            callback.from_user.id,
            file_id=results[3]
        )

        await client.send_message(
            chat_id=callback.from_user.id,
            text=_("<strong>Success!</strong>\n\n"
                   "We already had uploaded a copy of the version you "
                   "requested **(%s, %s)**, so we sent it to you instantly. "
                   "Stay safe!") % (platform, locale),
            reply_markup=InlineKeyboardMarkup(
            [
                [InlineKeyboardButton(
                    _("Main Menu"),
                    "welcome:" + lang
                )]
            ])
        )
    else:
        await logger.info(f"Binary ({platform}, {locale}) not found. Uploading...")

        await client.send_message(
            callback.from_user.id,
            _("The version you have requested **(%s, %s)** "
              "hasn't been uploaded to Telegram's servers yet. "
              "Please wait...") % (platform, locale)
        )

        reply = await client.send_message(
            callback.from_user.id,
            _("We are uploading the signature file first...")
        )

        tor_sig_id = await files.relay_files(
            client,
            callback,
            tor_sig_url,
            tor_sig_original_name,
            data['bot']['download_path'],
            reply,
            lang,
            logger
        )

        reply = await client.send_message(
            callback.from_user.id,
            _("We are now uploading the program...")
        )

        tor_bin_id = await files.relay_files(
            client,
            callback,
            tor_bin_url,
            tor_bin_original_name,
            data['bot']['download_path'],
            reply,
            lang,
            logger
        )

        if len(tor_sig_id) == 0 or len(tor_bin_id) == 0:
            await logger.info(f"Error during upload of file ({platform}, {locale}).")

            await callback.message.edit(
                text=_("<strong>Failure!</strong>\n\n"
                       "Something went wrong during the upload! "
                       "Please try again."
                ) + f"\n\n☎️: @TorProjectSupportBot ({platform}, {locale})",
                reply_markup=InlineKeyboardMarkup(
                [
                    [InlineKeyboardButton(
                        _("Main Menu"),
                        "welcome:" + lang
                    )]
                ])
            )

            return
        else:
            await database.insert_new_release(
                curs,
                conn,
                tor_bin_original_name,
                tor_bin_id,
                tor_sig_original_name,
                tor_sig_id
            )

            await conn.commit()

            await callback.message.edit(
                text=_("<strong>Success!</strong>\n\n"
                       "The version you have requested **(%s, %s)** has been "
                       "successfully uploaded and sent to you. "
                       "Stay safe!" % (platform, locale)
                ),
                reply_markup=InlineKeyboardMarkup(
                [
                    [InlineKeyboardButton(
                        _("Main Menu"),
                        "welcome:" + lang
                    )]
                ])
            )

            await logger.info(f"--- NEW ENTRY ---")
            await logger.info(f"Binary name: {tor_bin_original_name}: ")
            await logger.info(f"Binary cache ID: {tor_bin_id}: ")
            await logger.info(f"Signature name: {tor_sig_original_name}:")
            await logger.info(f"Signature cache ID: {tor_sig_id}")
            await logger.info(f"-----------------")

async def _get_me_loop(app):
    global bot_profile
    while True:
        try:
            bot_profile = await app.get_me()
        except:
            pass
        await asyncio.sleep(60)

async def main():
    global conn, curs, response

    '''
    We take care of all of the logging stuff before we
    do anything else. There definitely has to be some sort
    of a filename though, so we hardcode "OSB" in case
    nothing else is available.
    '''

    # We start off with the path where files will be stored.
    # If one hasn't been defined, it will be left blank and
    # the default directory will be the current working one.
    log_filename = str(data['logging']['directory'] or '')

    # Carrying on with the file name...
    if (data['logging']['filename_suffix'] is not None):
        log_filename += data['logging']['filename_suffix']
    else:
        log_filename += "OSB"

    # We concatenate the date and the time for better organization...
    # As always, this is optional, but configured by default.
    if (data['logging']['filename_datefmt'] is not None):
        # Adds separator between name and date, if defined.
        log_filename += str(data['logging']['date_separator'] or '')
        # Adds date depending on format defined in the configuration.
        log_filename += dt.now().strftime(data['logging']['filename_datefmt'])

    '''
    Here be dragons! aiologger works slightly differently than the `logging`
    module and is, as of the time of this writing, largely undocumented. The
    handlers are similar to the default handlers provided by the library,
    but we add them manually for a greater degree of control.
    '''

    log_format = Formatter(
        fmt = data['logging']['format'],
        datefmt = f"{data['logging']['datefmt']}"
    )

    logger.add_handler(
        AsyncStreamHandler(
            stream = sys.stderr,
            level = logging.INFO,
            formatter = log_format
        )
    )

    logger.add_handler(
        AsyncStreamHandler(
            stream = sys.stderr,
            level = logging.WARNING,
            formatter = log_format
        )
    )

    # TODO: Ask maintainers to add additional formatting options.
    logger.add_handler(
        AsyncFileHandler(
            filename = f'{log_filename}.log',
            encoding = data['logging']['encoding']
        )
    )

    # Okay, here comes the real deal.
    await logger.info("=== OnionSproutsBot ===")

    # Custom paths
    if (data['bot']['download_path'] is not None):
        await logger.info(f"Custom download path: {data['bot']['download_path']}")

    try:
        if (data['bot']['db_path'] is None):
            await logger.info(f"Database path: {os.getcwd()}/{data['bot']['db_name']}")
            conn = await aiosqlite.connect(f"{os.getcwd()}/{data['bot']['db_name']}")
        else:
            conn = await aiosqlite.connect(data['bot']['db_path'])
    except Exception as e:
        await logger.critical(e)
        exit(1)

    curs = await conn.cursor()

    await database.create_empty_db(curs, conn)

    # Obtains a list of available downloads on startup.
    response = requests.get(data['tor']['endpoint']).json()
    await logger.info("Initial response from endpoint obtained.")

    await conn.commit()
    await logger.info("Connection to database initiated.")

    # Call Pyrogram's idle forever function
    await OnionSproutsBot.start()

    # Start a get_me loop, this is really only needed if
    # the bot's information is edited as we're running.
    asyncio.create_task(_get_me_loop(OnionSproutsBot))

    await idle()
    await conn.close()

loop = asyncio.get_event_loop()

def init():
    loop.run_until_complete(main())
