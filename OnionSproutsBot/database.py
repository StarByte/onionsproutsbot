#!/usr/bin/env python3
#
# This file is part of OnionSpoutsBot, a Tor Browser distribution Telegram bot.
#
# :authors: Panagiotis "Ivory" Vasilopoulos <git@n0toose.net>
#           Justin "Justasic" Crawford <OpenSource@stacksmash.net>
#
# :copyright:   (c) 2020-2022, Panagiotis "Ivory" Vasilopoulos
#
# :license: This is Free Software. See LICENSE for license information.
#

import aiosqlite

async def search_file_in_db(cursor: aiosqlite.Cursor, filename: str):
    return await (await cursor.execute(
        f'''SELECT binary, binary_id, sig, sig_id FROM tor_releases WHERE (
             binary = "{filename}"
        );'''
    )).fetchone()

async def create_empty_db(cursor: aiosqlite.Cursor, connection: aiosqlite.Connection):
    await cursor.execute(
        '''CREATE TABLE IF NOT EXISTS tor_releases (
            binary TEXT, binary_id INTEGER, sig TEXT, sig_id INTEGER,
            UNIQUE(binary, binary_id, sig, sig_id)
        );'''
    )

    return await connection.commit()

async def insert_new_release(
    cursor: aiosqlite.Cursor,
    connection: aiosqlite.Connection,
    name1: str,
    id1: int,
    name2: str,
    id2: int
):
    await cursor.execute(
        f'''INSERT INTO tor_releases VALUES (
            '{name1}', '{id1}', '{name2}', '{id2}'
        );'''
    )

    return await connection.commit()
