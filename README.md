# OnionSproutsBot

Telegram bot for distributing software by The Tor Project.

## Usage Guide

- Install the required Python modules.

If you are planning to work with the source code, run
`pip3 install -r requirements.txt`. That should suffice on a system with
[Python 3.7+](https://python.org) and [pip](https://pypi.org/project/pip/)
installed. If applicable (mostly on Linux systems), you can also use Python
modules provided by software packages.

For example, on Debian, you can install the [`aiofiles`](https://pypi.org/project/aiofiles/)
dependency using `apt install python3-aiofiles`.

- Install the bot

In most cases, you can simply run `python3 setup.py install`. This will install
the Telegram bot on your system.

If you do not have administrative rights over your system or if your system
otherwise limits your ability to modify system folders (e.g. Fedora Silverblue,
Windows user without administrative premissions), you can use the
`--user` parameter to install the bot: `python3 setup.py install --user`

You will most likely have to figure out how to modify the `$PATH` environment
variable in your system in order to run the bot with the command `osbtg`
without entering the full directory in which the executable file is present in.
On a Linux system without such a modification, you would probably have
to use `$HOME/.local/bin/osbtg` to run the bot.

- Follow [Pyrogram's Project Setup Guide](https://docs.pyrogram.org/start/setup).

You will need a Telegram account, as well as a bot account that can also be
created with the help of [BotFather](https://t.me/BotFather). Make sure to
create an application on the [Telegram website](https://my.telegram.org).

- After you're done, edit `example.yaml`.

You will need to enter your API key, your application's API hash and your bot's
access token. Although this bot was designed to be used with a bot account,
you can also use an access token for a user account. There is also a set of
additional options, such as the default name and the location of the database
file, where all the ID numbers of cached files that have already been uploaded
to Telegram's servers are stored.

- If a `.session` file is not present, you will be asked to enter a phone
  number and an access code.

Make sure you're using an interactive terminal when running the bot for the first time. This will generate
a file with the extension `.session`, which you can also copy to other machines.
Make sure to keep that file secret!

- You can now run the bot using `osbtg example.yaml`!

### Mirrors & Issue Tracker

Currently, the bot is being developed on
[The Tor Project's GitLab instance](https://gitlab.torproject.org/tpo/anti-censorship/gettor-project/onionsproutsbot)
as a part of the [gettor project](https://gitlab.torproject.org/tpo/anti-censorship/gettor-project/).

If you do not wish to make an account, it is also possible to report issues
directly using email.

## Design & Documentation

Since the design of the bot is evolving constantly, there's currently no
formal documentation. However, the design and the thought process that have
gone into this are being extensively documented. The latest revision of
the code always includes comments that correspond to the problems that we
dealt with, and temporary countermeasures for problems that have not been
fully thought out yet will definitely be there until they are resolved.
Older commits may provide more context to certain design decisions.

- [Original thread at The Tor Project's GitLab](https://gitlab.torproject.org/tpo/anti-censorship/gettor-project/trac/-/issues/22011).
- [Issue Tracker](https://gitlab.torproject.org/tpo/anti-censorship/gettor-project/onionsproutsbot)

### What are the goals of this project?

Someone brought up to me once that other bots like mine have solved similar
problems. Even though similar implementations exist, they achieve wildly
different goals, **they are not the same.**

Here's a list of our goals in order to better describe what this project aims to be:

- Concurrent. Able to serve as many people as possible.
- Resilient. Mitigate as many attack vectors as possible.
- Expandable. Sustainable. Easily adaptable.
- New, complicated feature? Get it work, definitely clean up later.
- Low bandwidth, old devices, language barriers should not matter.
- Beginner-friendly. That way, it will also be maintainer-friendly.

### Why Pyrogram?

[Pyrogram](https://pyrogram.org), unlike most other libraries, does not
interact with [Telegram's Bot API](https://core.telegram.org/bots/api), but
uses the MTProto protocol instead, which is the library that
Telegram clients use. Using MTProto is of paramount importance, as it is bound
to the same limitations that an actual user is, which is also why a user
account (with a set of API keys) is also required.

For example, a user can upload files that are as large as 2 GB. However, a
simple bot can only upload files of up to 50 MBs.

I unfortunately found out about this the hard way, as I initially attempted to
write the bot using
[python-telegram-bot](https://github.com/python-telegram-bot/python-telegram-bot),
which is syntactically similar in many ways. That allowed me to copy and paste
most of the code, but I had to think about a lot of design decisions all over
again and, as Pyrogram isn't used as often as other competing libraries.
