# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-09-26 11:45-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: OnionSproutsBot/bot.py:93
msgid ""
"<strong>Language selection</strong>\n"
"\n"
"Please select your language:"
msgstr ""

#: OnionSproutsBot/bot.py:139
msgid "Change Language"
msgstr ""

#: OnionSproutsBot/bot.py:141
#, python-brace-format
msgid ""
"<strong>Welcome!</strong>\n"
"\n"
"Hi, welcome to {name}! What do you want me to do?"
msgstr ""

#: OnionSproutsBot/bot.py:149
msgid "Send me Tor Browser through Telegram"
msgstr ""

#: OnionSproutsBot/bot.py:153
msgid "Send me other mirrors for the Tor Browser"
msgstr ""

#: OnionSproutsBot/bot.py:157
msgid "I want Tor bridges"
msgstr ""

#: OnionSproutsBot/bot.py:161
msgid "Explain what Tor is"
msgstr ""

#: OnionSproutsBot/bot.py:188
msgid ""
"(You should also include your operating system: Windows, macOS, or Linux)"
msgstr ""

#: OnionSproutsBot/bot.py:189
msgid "(Make sure that your email provider is safe!)"
msgstr ""

#: OnionSproutsBot/bot.py:193 OnionSproutsBot/bot.py:220
#: OnionSproutsBot/bot.py:245 OnionSproutsBot/bot.py:378
#: OnionSproutsBot/bot.py:436 OnionSproutsBot/bot.py:464
msgid "Main Menu"
msgstr ""

#: OnionSproutsBot/bot.py:207
msgid ""
"<strong>Tor Bridges</strong>\n"
"\n"
"Tor bridges are computers that can be very helpful when your internet "
"provider has blocked you from using Tor, or if you just want to hide the "
"fact that you are using Tor from anyone that could be monitoring your "
"internet connection. That could be your internet provider, government, "
"household members, school, workplace, among others.\n"
"\n"
"You can request a bridge from @getbridgesbot, but you should also configure "
"the Tor Browser to use it."
msgstr ""

#: OnionSproutsBot/bot.py:235
msgid ""
"<strong>What is Tor?</strong>\n"
"\n"
"Tor is a network that lets you anonymously connect to the Internet, while "
"evading censorship, people monitoring of your Internet connection and ad "
"tracking.\n"
"\n"
"The network is peer-to-peer and largely supported by volunteers, as well as "
"The Tor Project Inc., a non-profit organization based in the United States."
msgstr ""

#: OnionSproutsBot/bot.py:281
msgid ""
"<strong>Download Tor from Telegram</strong>\n"
"\n"
"Okay! I'll ask you a couple of questions first.\n"
"<b>Which operating system are you using?</b>"
msgstr ""

#: OnionSproutsBot/bot.py:315
msgid ""
"<strong>Download Tor from Telegram</strong>\n"
"\n"
"Okay! I'll ask you a couple of questions first.\n"
"<b>What is your preferred language?</b>"
msgstr ""

#: OnionSproutsBot/bot.py:371
#, python-format
msgid ""
"<strong>Success!</strong>\n"
"\n"
"We already had uploaded a copy of the version you requested **(%s, %s)**, so "
"we sent it to you instantly. Stay safe!"
msgstr ""

#: OnionSproutsBot/bot.py:388
#, python-format
msgid ""
"The version you have requested **(%s, %s)** hasn't been uploaded to "
"Telegram's servers yet. Please wait..."
msgstr ""

#: OnionSproutsBot/bot.py:395
msgid "We are uploading the signature file first..."
msgstr ""

#: OnionSproutsBot/bot.py:411
msgid "We are now uploading the program..."
msgstr ""

#: OnionSproutsBot/bot.py:429
msgid ""
"<strong>Failure!</strong>\n"
"\n"
"Something went wrong during the upload! Please try again."
msgstr ""

#: OnionSproutsBot/bot.py:456
#, python-format
msgid ""
"<strong>Success!</strong>\n"
"\n"
"The version you have requested **(%s, %s)** has been successfully uploaded "
"and sent to you. Stay safe!"
msgstr ""

#: OnionSproutsBot/files.py:44 OnionSproutsBot/files.py:64
#, python-format
msgid "%d second"
msgid_plural "%d seconds"
msgstr[0] ""
msgstr[1] ""

#: OnionSproutsBot/files.py:50
#, python-format
msgid "%d day"
msgid_plural "%d days"
msgstr[0] ""
msgstr[1] ""

#: OnionSproutsBot/files.py:54
#, python-format
msgid "%d hour"
msgid_plural "%d hours"
msgstr[0] ""
msgstr[1] ""

#: OnionSproutsBot/files.py:59
#, python-format
msgid "%d minute"
msgid_plural "%d minutes"
msgstr[0] ""
msgstr[1] ""

#: OnionSproutsBot/files.py:124
#, python-format
msgid ""
"<b>We are preparing your download...</b>\n"
"\n"
"We're now sending the file to Telegram, it will be ready in %s!\n"
msgstr ""

#: OnionSproutsBot/files.py:129
msgid ""
"<b>We are preparing your download...</b>\n"
"\n"
"You're the first person to request this file, so we have to download it "
"first! Please wait...\n"
msgstr ""

#: OnionSproutsBot/files.py:194
#, python-format
msgid "Upload failed! Please try again later. Reason: `%s`"
msgstr ""
