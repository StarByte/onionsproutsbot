#!/bin/bash

CWD="."

if [ -d "locales" ]; then
    if [ -d "OnionSproutsBot" ]; then
      echo "Already possibly in top directory, proceeding..."
    else
      echo "Not in directory, exiting..."
      exit 1
    fi
else
  if [ -d "../locales" ]; then
    CWD=".."
  else
    echo "Not in directory, exiting..."
    exit 1
  fi
fi

echo "Checking if repository already exists..."

if [ -d "$CWD/../translation" ]; then
  echo "Folder exists. Trying to update repository..."
  git -C $CWD/../translation pull
else
  echo "Folder does not exist. Cloning..."
  # TODO: Eventually replace with completed translations only.
  git -C $CWD/.. \
    clone git@gitlab.torproject.org:tpo/translation.git \
    --branch=onionsproutsbot
fi

echo "Copying and pasting localization files..."
cp $CWD/../translation/onionsproutsbot+*.po locales/

echo "Done."
exit 0
