from setuptools import setup, find_packages
import sys

assert sys.version_info >= (3, 6, 0)

from pathlib import Path
from typing import List
from pkg_resources import parse_requirements
import os
import subprocess

name = "OnionSproutsBot"
current_dir = Path(__file__).parent
sys.path.insert(0, str(current_dir))

def get_install_requires() -> List[str]:
    with Path(current_dir / 'requirements.txt').open() as requires:
        return [str(pkg) for pkg in parse_requirements(requires)]

def get_long_description() -> str:
    return (
        (current_dir / "README.md").read_text(encoding="utf8")
    )

# Compiles all the translations, creates a list of them and formats them so they can be accepted as data_files.
def create_catalogs():
    catalogs = []
    locale_dir = Path('locales')
    for lang in list(locale_dir.glob('*.po')):
        file_without_suffix = lang.with_suffix('') # Used more than once
        # Just the locale name: "locales/onionsproutsbot+el" => "el"
        locale = file_without_suffix.name.split('+')[-1]
        # Output path: "locales/el/LC_MESSAGES/onionsproutsbot.mo"
        # with_name is being used cause we need the stem ("locales/")
        # can be replaced with `locale_dir, locale, ...`
        output = Path(file_without_suffix.with_name(locale), "LC_MESSAGES", "onionsproutsbot.mo")
        output.parent.mkdir(parents=True, exist_ok=True)
        p = subprocess.run(['msgfmt', str(lang), '-o', str(output)], capture_output=True)
        # msgfmt might error on warns, so better check if it compiled rather than depend on it.
        if output.exists():
            catalogs.append(
                (os.path.join("OnionSproutsBot", "locales", locale, "LC_MESSAGES"),
                 [str(output)]))
    return catalogs

setup(
    name=name,
    version="1.1.0",
    author='Panagiotis "Ivory" Vasilopoulos',
    author_email="git@n0toose.net",
    description="Telegram distribution bot for Tor",
    long_description=get_long_description(),
    long_description_content_type="text/markdown",
    packages=find_packages(),
    python_requires='>=3.6',
    install_requires=get_install_requires(),
    data_files=create_catalogs(),
    zip_safe=False,
    entry_points={
        'console_scripts': [
            f'osbtg={name}.bot:init'
        ]
    },
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.10",
    ],
)
